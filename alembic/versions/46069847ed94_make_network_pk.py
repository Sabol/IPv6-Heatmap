"""make network pk

Revision ID: 46069847ed94
Revises: d9fe2161434
Create Date: 2015-10-22 01:07:18.975099

"""

# revision identifiers, used by Alembic.
revision = '46069847ed94'
down_revision = 'd9fe2161434'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
            'IPV6GEODATA',
            sa.Column('network', sa.String(50), primary_key=True),
            sa.Column('geoname_id', sa.String(50), nullable=True),
            sa.Column('registered_country_geoname_id', sa.String(50), nullable=True),
            sa.Column('represented_country_geoname_id', sa.String(50), nullable=True),
            sa.Column('is_anonymous_proxy', sa.String(50), nullable=True),
            sa.Column('is_satellite_provider', sa.String(50), nullable=True),
            sa.Column('postal_code', sa.String(50), nullable=True),
            sa.Column('latitude', sa.Integer, nullable=False),
            sa.Column('longitude', sa.Integer, nullable=False)
    )

def downgrade():
    pass
