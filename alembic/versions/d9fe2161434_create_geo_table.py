"""create geo table

Revision ID: d9fe2161434
Revises: 
Create Date: 2015-10-21 16:11:42.536361

"""

# revision identifiers, used by Alembic.
revision = 'd9fe2161434'
down_revision = None
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
            'ipv6geo',
            sa.Column('network', sa.String(50), nullable=True),
            sa.Column('geoname_id', sa.String(50), nullable=True),
            sa.Column('registered_country_geoname_id', sa.String(50), nullable=True),
            sa.Column('represented_country_geoname_id', sa.String(50), nullable=True),
            sa.Column('is_anonymous_proxy', sa.String(50), nullable=True),
            sa.Column('is_satellite_provider', sa.String(50), nullable=True),
            sa.Column('postal_code', sa.String(50), nullable=True),
            sa.Column('latitude', sa.Integer, nullable=False),
            sa.Column('longitude', sa.Integer, nullable=False)
    )

def downgrade():
    pass
