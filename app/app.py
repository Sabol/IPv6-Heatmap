from flask import Flask, request, render_template, jsonify, Response
from flask.ext.sqlalchemy import SQLAlchemy

app = Flask(__name__, static_url_path='/static')
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///../data/ipv6geo.db'
db = SQLAlchemy(app)
session = db.session
db.Model.metadata.reflect(db.engine)

# Source the confiuration properties
app.config.from_object('config')

"""
For simplicity's sake, I just decided to declare my model and my rest resource
in the app file. It's such a small app, so there is no need to create a complicated structure.
"""
# Define a model from an already existing db table
class Ipv6geoData(db.Model):
    __table__ = db.Model.metadata.tables['IPV6GEODATA']

    def __repr__(self):
        # The app will only ever need these values, so let's short cut some stuff.
        return '<%s,%s>' % (self.latitude, self.longitude)

# RESTful geo data endpoint
# only handle GET's, 400 res on other methods?
# Although ideally the endpoint should handle other methods
# for general use purposes
@app.route('/geodata')
def ipv6geodata():
    # Handle HTTP verbs here.
    if request.method == 'GET':
        ip_data = None
        ip_data_dict = {}
        # We need to handle the data sent with the request
        # print request.args
        # dict is true if not empty and false if empty
        if request.args:
            res = request.args 
            print res.keys()

            # Check the keys of the request args to make sure the right
            # ones were sent over
            if res.keys() == ['lngmin', 'lngmax', 'latmin', 'latmax']:
                print 'I have the keys!!'
                # Construct a query for the data from the database
                q = session.query(Ipv6geoData)\
                    .filter(Ipv6geoData.longitude > res['lngmin'],
                        Ipv6geoData.longitude < res['lngmax'],
                        Ipv6geoData.latitude > res['latmin'],
                        Ipv6geoData.latitude < res['latmax'])
                # Execute the query
                ip_data = q.all()
                
                # gotta convert the data returned from the db into something that flask can
                # send to the client, i.e jsonify-able data so a dictionary. Can't return a 
                # list for security reasons (note to self: look up why exactly...)
                count = 0
                for i in q:
                    # get the lat and long returned from our db model
                    latitude = i.latitude
                    longitude = i.longitude

                    ip_data_dict[count] = {'latitude': latitude, 'longitude': longitude}
                    count = count + 1
            else:
                return render_template('oops.html')
        else:
            return render_template('oops.html') 

        return jsonify(ip_data_dict) 

# Route to web app
@app.route('/')
def ipv6Map():
    return render_template('index.html')

if __name__ == '__main__':
    #app.run(debug=True)
    app.run()
