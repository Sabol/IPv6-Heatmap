## IPv6 Heatmap
### Author: Cory Sabol - cssabol@uncg.edu

This is a rather small application written using python and flask to define a REST and JavaScript
coupled with Mapbox.js to create an interactive map visualizing the worlds ipv6 data.

### Usage:

To access the app itself [click here](http://cssabol.pythonanywhere.com)

On the front end simply click the small filled in sqaure icon on the left side of the map and click and drag the mouse
to select an area for which to render a heatmap of ipv6 geolocational points. I made the decision to render the data this way
so that I wouldn't have to render it all at once or constanly query the REST api each time the map was moved or zoomed.

#### Warning: This app was built by me in roughly 10 hours total and is not very optimized. As such selecting large areas may lock up your browser.

### REST API Andpoint:
To test out the api endpoint itself use the url: 
    `http://cssabol.pythonanywhere.com/geodata?lngmin=<num>&lngmax=<num>&latmin=<num>&latmax=<num>`
of course you have to fill in the <num>'s with geographic coordinates.
The endpoint returns a json string containg geographic coordinates of all the IPv6 addresses that fall within
the geographic bounding box sent to the server.

### Setup:

If you want to set the app up on your local machine you will need to do the following:

1. run `source env/bin/activate` to source the virtual env (you may need to install python virtualenv) 
2. From the root of the project run python app/app.py

### Tools used:

1. Python with Flask - provides the routing and REST endpoint
2. Mapbox - a framework built on top of Leaflet that offers more features
3. Leaflet.heat - Leaflet plugin to render heatmaps
4. GeoLite2 ip geo location database - the source of data for the application
5. SQLite3 - the database containing the ipv6 geo data
6. SQLAlchemy for python - database helper library
7. Alembic - manage ORM models and database revisions 

### Things ToDo:

1. Client-side performance - this is a big one!
2. Understanding of data earlier on in the process.
3. Use protocol buffers
4. Write tests
5. Make the client ui more intuitive
